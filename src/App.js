import React from 'react';
import Navigation from './Navigation';
import { Provider } from 'react-redux';
import { store, persistor } from './store';
import { PersistGate } from 'redux-persist/es/integration/react';
import { Spinner } from '@blueprintjs/core';

import 'normalize.css';
import './styles.css';

function App() {
  return (
    <div className="app">
      <Provider store={store}>
        <PersistGate loading={<Spinner intent="primary" size={100}/>} persistor={persistor}>
          <Navigation />
        </PersistGate>
      </Provider>
    </div>
  );
}

export default App;
