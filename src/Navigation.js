import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Home from './containers/Home';
import PostDetail from './containers/PostDetail';
import Navbar from './components/Navbar';

export default function Navigation() {
  return (
    <>
      <Router>
        <Navbar />
        <Switch>
          <Route path="/post/:id">
            <PostDetail />
          </Route>
          <Route path="/">
            <Home />
          </Route>
        </Switch>
      </Router>
    </>
  );
}
