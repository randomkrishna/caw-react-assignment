import { takeLatest, put, all, call } from 'redux-saga/effects';
import {
  GET_ALL_BLOG_POST_REQUEST,
  GET_ALL_BLOG_POST_SUCCESS,
  GET_POST_DETAIL_REQUEST,
  GET_POST_DETAIL_SUCCESS,
  GET_POST_COMMENT_REQUEST,
  GET_POST_COMMENT_SUCCESS,
} from '../reducers/actionTypes';
import { getBlogPost, getPostDetails, getPostComment } from '../api';
import { Position, Toaster, Intent } from '@blueprintjs/core';

/** Singleton toaster instance. Create separate instances for different options. */
export const AppToaster = Toaster.create({
  className: 'recipe-toaster',
  position: Position.TOP,
  intent: Intent.DANGER,
  icon: 'warning-sign',
});

function* getAllPost() {
  try {
    const { data } = yield call(getBlogPost);
    yield put({
      type: GET_ALL_BLOG_POST_SUCCESS,
      payload: data,
    });
  } catch (error) {
    console.log(error);
    AppToaster.show({ message: 'Cannot get posts.' });
  }
}

function* getPost({ payload }) {
  try {
    const { data } = yield call(getPostDetails, payload);
    yield put({
      type: GET_POST_DETAIL_SUCCESS,
      payload: data,
    });
  } catch (error) {
    console.log(error);
    AppToaster.show({ message: "Cannot get post details, you'll be redirected." });
    setTimeout(() => window.location.href = '/', 2000);
  }
}

function* getComment({ payload }) {
  try {
    const { data } = yield call(getPostComment, payload);
    yield put({
      type: GET_POST_COMMENT_SUCCESS,
      payload: data,
    });
  } catch (error) {
    console.log(error);
    AppToaster.show({ message: 'Cannot get post comments' });
  }
}

export default function* root() {
  yield all([
    takeLatest(GET_ALL_BLOG_POST_REQUEST, getAllPost),
    takeLatest(GET_POST_DETAIL_REQUEST, getPost),
    takeLatest(GET_POST_COMMENT_REQUEST, getComment),
  ]);
}
