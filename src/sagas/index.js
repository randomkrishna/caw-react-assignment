import {all, call} from 'redux-saga/effects';
import PostSaga from './post.saga';

export function* rootSaga() {
  yield all([call(PostSaga)]);
}