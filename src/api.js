
import axios from 'axios';

const api = axios.create({
  baseURL: "https://jsonplaceholder.typicode.com",
  timeout: 10000,
});

export const getBlogPost = () => {
  return api.get('/posts');
};

export const getPostDetails = (id = 1) => {
  return api.get(`/posts/${id}`);
};

export const getPostComment = (id = 1) => {
  return api.get(`comments?postId=${id}`);
};
