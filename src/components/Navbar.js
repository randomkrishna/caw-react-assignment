import React from 'react';
import { Alignment, Button, Navbar } from '@blueprintjs/core';
import { Link } from 'react-router-dom';

export default function NavbarComponent() {
  return (
    <Navbar>
      <Navbar.Group align={Alignment.LEFT}>
        <Navbar.Heading>Blog App</Navbar.Heading>
        <Navbar.Divider />
        <Link to="/">
          <Button icon="home" minimal>
            Home
          </Button>
        </Link>
      </Navbar.Group>
    </Navbar>
  );
}
