import * as actionTypes from './actionTypes';

export const actions = {
  postRequest: (payload) => ({
    type: actionTypes.GET_ALL_BLOG_POST_REQUEST,
  }),
  postSuccess: (payload) => ({
    type: actionTypes.GET_ALL_BLOG_POST_SUCCESS,
    payload,
  }),
  postDetailRequest: (payload) => ({
    type: actionTypes.GET_POST_DETAIL_REQUEST,
    payload,
  }),
  postDetailSuccess: (payload) => ({
    type: actionTypes.GET_POST_DETAIL_SUCCESS,
    payload,
  }),
  postDetailReset: () => ({
    type: actionTypes.RESET_POST_DETAIL,
  }),
  postCommentRequest: (payload) => ({
    type: actionTypes.GET_POST_COMMENT_REQUEST,
    payload,
  }),
  postCommentSuccess: () => ({
    type: actionTypes.GET_POST_COMMENT_SUCCESS,
  }),
};

const initialState = {
  posts: [],
  updateTime: undefined,
  activePost: {},
  postComments: [],
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.GET_ALL_BLOG_POST_SUCCESS: {
      return {
        ...state,
        posts: action.payload,
        updateTime: Date.now(),
      };
    }
    case actionTypes.GET_POST_DETAIL_SUCCESS: {
      return {
        ...state,
        activePost: action.payload,
      };
    }
    case actionTypes.RESET_POST_DETAIL: {
      return {
        ...state,
        activePost: {},
        postComments: [],
      };
    }
    case actionTypes.GET_POST_COMMENT_SUCCESS: {
      return {
        ...state,
        postComments: action.payload,
      };
    }
  }

  return state;
};

export default reducer;
