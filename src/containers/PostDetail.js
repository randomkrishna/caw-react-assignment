import React, { useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { actions as postsActions } from '../reducers/post.reducer';
import { Card, H3, Elevation, Spinner, Button, H5, Divider, H6 } from '@blueprintjs/core';

export default function PostDetail() {
  const dispatch = useDispatch();
  const postDetail = useSelector((state) => state.postReducer.activePost);
  const postComment = useSelector((state) => state.postReducer.postComments);

  const fetchComments = () => dispatch(postsActions.postCommentRequest(id));

  const { id } = useParams();

  const resetPrevState = () => {
    dispatch(postsActions.postDetailReset());
  };

  useEffect(() => {
    // clearing state for unwanted data
    resetPrevState();
    dispatch(postsActions.postDetailRequest(id));
    return () => {
      resetPrevState();
    };
  }, []);

  useEffect(() => {
    // could be also done by using react-helmet lib
    if (postDetail?.title && document) {
      document.title = postDetail.title;
    }
  }, [postDetail]);

  return (
    <div className="padding15">
      {!postDetail?.id ? (
        <Spinner intent="primary" size={50} />
      ) : (
        <>
          <Card>
            <H3>{postDetail.title}</H3>
            <H5>{postDetail.body}</H5>
            {Array.isArray(postComment) && postComment.length ? (
              <>
                <Divider />
                <H5>{postComment.length} comments found</H5>
                {postComment.map((comment, index) => (
                  <div key={comment.id}>
                    <Card interactive={true} elevation={Elevation.TWO}>
                      <H6>Author: {comment.name}</H6>
                      <p>Email: {comment.email}</p>
                      <p>Comment: {comment.body}</p>
                    </Card>
                    {index < postComment.length - 1 ? <Divider /> : null}
                  </div>
                ))}
              </>
            ) : (
              <Button icon="comment" className="refreshBtn" onClick={fetchComments}>
                Load comments
              </Button>
            )}
          </Card>
        </>
      )}
    </div>
  );
}
