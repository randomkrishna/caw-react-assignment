import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { actions as postsActions } from '../reducers/post.reducer';
import { Card, Classes, H5, Elevation, Divider, Button, Spinner } from '@blueprintjs/core';
import { Link } from 'react-router-dom';
import debounce from 'lodash.debounce';

export default function Home() {
  const dispatch = useDispatch();
  const [searchTerm, setSearchTerm] = useState();
  const [searchResult, setSearchResult] = useState([]);
  const allPosts = useSelector((state) => state.postReducer.posts);
  const refreshTime = useSelector((state) => state.postReducer.updateTime);

  const searchPost = (e) => {
    setSearchTerm(e.target.value);
  };

  const debounceSearch = debounce(searchPost, 300);

  const refreshPosts = () => dispatch(postsActions.postRequest());

  useEffect(() => {
    !refreshTime && refreshPosts();
  }, []);

  useEffect(() => {
    if (searchTerm && searchTerm.length > 2) {
      const searchResult = allPosts.filter((p) =>
        p.title.toLowerCase().includes(searchTerm.toLowerCase())
      );
      setSearchResult(searchResult);
    } else if (searchResult.length) {
      setSearchResult([]);
    }
  }, [searchTerm]);

  const postToShow =
    searchResult.length && searchTerm && searchTerm.length > 2 ? searchResult : allPosts;

  return (
    <>
      <div className="padding15">
        <div className="bp3-input-group">
          <span className="bp3-icon bp3-icon-search"></span>
          <input
            className="bp3-input"
            type="search"
            placeholder="Type to search, min 3 characters"
            dir="auto"
            onChange={debounceSearch}
          />
        </div>
      </div>
      <div className="padding15">
        {refreshTime ? (
          <div className="info-box">
            <div className="search-result-detail">
              {searchTerm && searchTerm.length > 2 ? `${searchResult.length} result found` : null}
            </div>
            <div className="refresh-block">
              <span className="updated-time">
                Updated on: {new Date(refreshTime).toLocaleString()}{' '}
              </span>
              <Button icon="refresh" className="refreshBtn" small onClick={refreshPosts}>
                Refresh
              </Button>
            </div>
          </div>
        ) : null}
        {!refreshTime ? (
          <Spinner intent="primary" size={50} />
        ) : (
          <>
            {postToShow.map((post) => (
              <div key={post.id}>
                <Card interactive={true} elevation={Elevation.TWO}>
                  <H5>{post.title}</H5>
                  <p>{post.body}</p>
                  <Link to={`/post/${post.id}`} className={Classes.BUTTON}>
                    Read More
                  </Link>
                </Card>
                <Divider />
              </div>
            ))}
          </>
        )}
      </div>
    </>
  );
}
